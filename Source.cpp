#include "Adapter.h"
#include "Composite.h"
#include <iostream>
#include "Proxy.h"
#include "Facade.h"

int main(){

	/*adapter example*/
	std::cout << "adapter example:\n\n";
	Enemy troll("Troll");
	Enemy spider("Spider");

	Hero hero;

	troll.Attack(hero);
	spider.Attack(hero);

	hero.GetEnemyUnderControl(troll, spider);
	hero.GetEnemyUnderControl(spider, troll);

	troll.Attack(hero);
	spider.Attack(hero);
	/**/

	/*composite example*/
	std::cout << "\n\ncomposite example:\n\n";
	std::string x("sword");

	CraftComponent * sword = new CompositeComponent("The Sword of Thousand Truths");
	CraftComponent * blade = new CompositeComponent("Sword blade");
	CraftComponent * grip = new CompositeComponent("Sword grip");

	CraftComponent * metall_1 = new SimpleComponent("metall");
	CraftComponent * runes = new SimpleComponent("runes");
	CraftComponent * metall_2 = new SimpleComponent("metall");
	CraftComponent * skull = new SimpleComponent("Skull");

	blade->Add(metall_1);
	blade->Add(runes);
	
	grip->Add(metall_2);
	grip->Add(skull);

	sword->Add(blade);
	sword->Add(grip);

	std::cout << "result: " << sword->Operation();

	delete sword;
	delete blade;
	delete grip;
	delete metall_1;
	delete runes;
	delete metall_2;
	delete skull;
	/**/

	/*proxy example*/
	std::cout << "\n\nproxy example:\n\n";
	DungeonProxy dungeon(4, 15);

	std::vector<Player*> players;

	players.push_back(new Player(20));
	players.push_back(new Player(10));
	players.push_back(new Player(19));
	players.push_back(new Player(16));
	players.push_back(new Player(18));

	for (auto player : players){
		dungeon.EnterToDungeon(player);
	}

	dungeon.EnterToDungeon(players[3]);

	std::cout << dungeon;

	for (auto player : players){
		delete player;
	}
	/**/

	/*facade example*/
	std::cout << "\n\nfacade example:\n\n";
	LevelLoadFacade fc;
	fc.StartLevel();
	/**/

	return 0;
}