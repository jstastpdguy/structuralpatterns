#pragma once
#include <iostream>
#include <list>

/*
*  class provides general methods for composit and simle objects
*/
class CraftComponent{
protected:
	CraftComponent *parent_;
	std::string name_;
public:
	virtual ~CraftComponent(){}

	/*set name of object*/
	CraftComponent(std::string const & name) : name_(name){
		parent_ = nullptr;
	};

	/*set reference to parent in parent_ field*/
	void SetParent(CraftComponent *parent){
		this->parent_ = parent;
	}

	/*return parent of object*/
	CraftComponent * GetParent() const{
		return this->parent_;
	}

	/*for working with composite objects*/
	virtual void Add(CraftComponent* comp){}
	virtual void Remove(CraftComponent* comp){}

	/*return complexity of object*/
	virtual bool IsComposite() const{
		return false;
	}

	/*some operation */
	virtual std::string Operation() const = 0;
};

/* simple component hasn't nested components
	usually do all work, whereas other components delegate work
*/
class SimpleComponent : public CraftComponent{
public:

	SimpleComponent(std::string const && name) : CraftComponent(name){};

	std::string Operation() const override{
		return name_;
	}
};

/*
	composite components may contain simple components and other composite components
	delegate work to children
*/
class CompositeComponent : public CraftComponent{
protected:
	std::list<CraftComponent*> children_;

public:

	CompositeComponent(std::string const && name) : CraftComponent(name){}

	/*add new component to children list
	and set its parent to self
	*/
	void Add(CraftComponent* component) override{
		this->children_.push_back(component);
		component->SetParent(this);
	}

	/*remove component from children list
	and set its parent to nullptr
	*/
	void Remove(CraftComponent * comp) override{
		children_.remove(comp);
		comp->SetParent(nullptr);
	}

	bool IsComposite() const override{
		return true;
	}

	/*delegate work to all children and sum it's results*/
	std::string Operation() const override{
		std::string result;

		for(const CraftComponent* c : children_){
			if(c == children_.back()){
				result += c->Operation();
			}else{
				result += c->Operation() + "+";
			}
		}

		return name_ + ": (" + result + ')';
	}
};