#pragma once
#include <iostream>

/*
	simple class that provides to alive creatures health points
*/
class Health{

	float amount = 100.f;

public:

	/*
		change health amount by value
		return how much hp left
	*/
	float ChangeHealth(float value){
		return amount += value;
	};

	/*
		puts health amount in ostream buf
	*/
	friend std::ostream& operator << (std::ostream& os, const Health& hp){
		return os << hp.amount;
	}
};

/*forward declaration of class*/
class Enemy;

/*
* player hero class
* has hp
* can tame enemies
*/
class Hero{

	Health hp;

public:

	void ChangeHealth(float value){
		if (hp.ChangeHealth(value) < 0){
			std::cout << "Hero is dead!";
		}else std::cout << "Hero is " << hp << "hp\n";
	}

	/*makes enemie fight on own side*/
	void GetEnemyUnderControl(Enemy& ToTame, Enemy& ToAttack);
};

/*
* has health, can attack hero
*/
class Enemy{
protected:

	Health hp;
	float attack = 30.f;
	std::string* name_ = nullptr;

public:

	/*
	* contructor which allocate memory for name string
	*/
	Enemy(std::string name) {
		name_ = new std::string(name);
	};

	void ChangeHealth(float value){
		if (hp.ChangeHealth(value) < 0){
			std::cout << *name_ << " is dead!";
		}else std::cout << *name_ << " is " << hp << "hp\n";
	}

	/*
		can attack only player hero
	*/
	void Attack(Hero& target){
		std::cout << *name_ << " attack ";
		target.ChangeHealth(-attack);
	}

	/*returns reference to enemy's name */
	std::string& GetName(){ return *name_; };

	/*free memory from name string*/
	~Enemy(){
		delete name_;
	}
};

/*
	same as enemy, but cannot fight against hero
*/
class Minion{
protected:

	Health hp;
	float attack = 30.f;
	std::string* name_ = nullptr;

public:

	Minion(){
		name_ = nullptr;
	}

	/*
	* contructor which allocate memory for name string
	*/

	Minion(std::string name) {
		name_ = new std::string(name);
	};

	void ChangeHealth(float value){
		if (hp.ChangeHealth(value) < 0){
			std::cout << *name_ << " is dead!";
		}else std::cout << *name_ << " - " << hp << '\n';
	}

	/*
		can attack only enemies
	*/
	virtual void Attack(Enemy& target){
		std::cout << *name_ << " attack ";
		target.ChangeHealth(-attack);
	}

	/*free memory from name string*/
	~Minion(){
		delete name_;
	}
};

/*
	makes enemies can attack themselves, 
	by override minion's attack function
*/
class Adapter : public Minion{
	Enemy* ToTame_;
public:

	/*
	* set Enemy to be tamed
	*/
	Adapter(Enemy* other){
		ToTame_ = other;
	};

	/*
	* make enemy attack other enemy by target ref
	*/
	void Attack(Enemy& target) override {
		std::cout << ToTame_->GetName() << " attack ";
		target.ChangeHealth(-this->attack);
	}

	~Adapter(){
		ToTame_ = nullptr;
	}
};

inline void Hero::GetEnemyUnderControl(Enemy& ToTame, Enemy& ToAttack){
		Adapter* tame = new Adapter(&ToTame);
		tame->Attack(ToAttack);
}