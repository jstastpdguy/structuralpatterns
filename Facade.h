#pragma once
#include <iostream>

class PlayerController{
public:
	void InitController(){
		std::cout << "controller created\n";
	};

	void PossessPawn(class PlayerPawn & pawn){
		std::cout << "controller: pawn was possessed\n";
	}
};

class PlayerPawn{
public:
	void InitPawn(){
		std::cout << "pawn created\n";
	};
};

class Level{
public:
	void InitLevel(){
		std::cout << "level created\n";
	}

	void SpawnPlayer(PlayerController const &){
		std::cout << "level: player was spawned\n";
	}
};

class LevelLoadFacade{
protected:
	std::unique_ptr<PlayerController> pc_;
	std::unique_ptr<PlayerPawn> pawn_;
	std::unique_ptr<Level> level_;
public:
	LevelLoadFacade(){
		pc_ = std::make_unique<PlayerController>();
		pawn_ = std::make_unique<PlayerPawn>();
		level_ = std::make_unique<Level>();
	}

	void StartLevel(){
		level_->InitLevel();
		pc_->InitController();
		pawn_->InitPawn();
		pc_->PossessPawn(*pawn_);
		level_->SpawnPlayer(*pc_);
	}
};