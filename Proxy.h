#pragma once
#include <vector>
#include <string>
#include <iostream>

/*default player names for initialization*/
std::string Players[] = {"Adam", "Mike", "Joe", "Olivia", "Jane"};

/*class of player, which has level, name, and id*/
class Player{
protected:
	int level_;
	std::string name_;
	static int playerId_;
public:

	/*constructor that get player level, and set name, by id*/
	Player(int level) : level_(level){ name_ = Players[playerId_++]; }

	/*getter for level*/
	int GetLevel(){ return level_; }
	/*getter for name*/
	std::string GetName() { return name_; }
};

/*player id, changes by making new player*/
int Player::playerId_ = 0;

/*players can enter the dungeon
* class contains player list, max players  for dubgeon and monsters level
*/
class Dungeon{
protected:
	std::vector<Player*> players_;
	int MaxPlayers = 0;
	int DungeonLevel = 0;
public:

	/*make dungeon with certain players count and monsters level*/
	Dungeon(int max_players, int dungeon_level) : MaxPlayers(max_players), DungeonLevel(dungeon_level){};

	/*allow players to enter, if dungeon doesn't full*/
	void EnterToDungeon(Player* player){
		if(players_.size() < MaxPlayers){
			players_.push_back(player);
			std::cout << player->GetName() << " enter to the dungeon\n";
		}else{
			std::cout << player->GetName() << ", you can't enter to the dungeon. It's full!\n";
		}
	}

	/*show player names in dungeon*/
	friend std::ostream& operator << (std::ostream &os, Dungeon const & dungeon){
		os << "Players in dungeon: ";
		for (auto player : dungeon.players_){
			os << player->GetName() << ' ';
		}
		return os;
	}

	/*free mem from vector of players*/
	~Dungeon(){ 
		for (auto player : players_) {
			player = nullptr;	
		}

		players_.empty();
	}
};

/*proxy for default dungeon class, which adittional check level of player*/
class DungeonProxy : public Dungeon{
	
public:

	DungeonProxy(int max_players, int dungeon_level) : Dungeon(max_players, dungeon_level){};

	/* doesn't allow player to enter, if his level too low*/
	void EnterToDungeon(Player* player){
		if(player->GetLevel() >= DungeonLevel){
			Dungeon::EnterToDungeon(player);
		}else 
			std::cout << player->GetName() << ", dungeon level too high, it may be difficult for you!\n";
	}
};